% Fake Book
% Arthur Reeder
% &copy 2019 Arthur

# CONTENIDO

[Introduccion al "Outside Public Contact" OPC](#introduccion)
[Tecnicismos del promotor](#tecnicismos)
[Conceptos del Tiempo Compartido](#)
[Tu tiempo y tu locacion](#)
[20 consejos utiles para el exito](#)
[Elementos para ser un verdadero vendedor](#)
[Pasos para obtener una Q](#)
  [1. Detener a la pareja](#)
  [2. Romper el hielo](#)
  [3. Calificar a la pareja](#)
  [4. Pichar regalo](#)
  [5. Pichar resort](#)
  [6. Llenar invitacion](#)
  [7. Cobrar deposito y establecer pickup](#)
  [8. Cerrar invitacion](#)
[Como obtener una directa](#)
[Como usar los convertidores](#)
[Como usar la psicologia en la promocion](#)
[Llamada de confirmacion y seguimiento](#)
[Atractivos turisticos de Quintana Roo y Yucatan](#)
[Venciendo objeciones](#)
[Juntemos un pitch](#)
[Herramientas del promotor](#)
[¿Como aumentar mi produccion?](#)
[¿Cuanto quieres ganar hoy?](#)
[Competencia](#)
[Establecimiento de metas](#)
[Trabajo en equipo](#)
[Ser el mejor en tu trabajo](#)
[¿Eres un ganador o perdedor?](#)
[Conviertete en lider y crece](#)

<a name="introduccion"></a>

# Introducción a “Outside Public Contact” (OPC)

En este manual aprenderás a tener éxito y superar tus habilidades como promotor de tiempo compartido. Para lograr el éxito necesitas tener el deseo de aprender. El conocimiento se obtiene leyendo este manual una y otra vez, poniendo én práctica lo aprendido para obtener la experiencia. Aun después de conocer bien el trabajo, deberás de consultarlo para “afilar” tus habilidades y refrescar tu mente con un buen repaso. La base del éxito de un promotor, es tener una Actitud Mental Positiva. Así mismo, deberás de rodearte de promotores de éxito y aprender de su experiencia. Esta profesión no es para perdedores. ¡Es para ganadores!

## Conocimientos + Acciones = EXITO

El proceso operativo de comercialización de membresías de tiempo compartido, se lleva a cabo a través de dos departamentos: **Mercadotecnia y Ventas.**

El departamento de mercadotecnia está encargado de contactar posibles compradores y lograr que atiendan una presentación de ventas. Para lograrlo, se llevan a cabo una gran variedad de promociones, a través de diversos puntos de venta (locaciones), distribuidos en los lugares con más tráfico de turistas. A esta labor se le llama *prospección*.

Las promociones incluyen boletos gratis o descuentos para tours, paseos en barco, espectáculos, deportes acuáticos, renta de autos, descuentos en tiendas o cenas de cortesía, camisetas, sarapes, botellas de licor, certificados de regalo, etc. Estos descuentos y regalos, se llaman **CONVERTIDORES**.

El departamento de ventas tiene el reto de atender a los clientes generados por el departamento de mercadotécnica y venderles una membresía de tiempo compartido. Este proceso funciona de la siguiente manera:

1. **El Promotor (OPC):** Invita a posibles. compradores a conocer el resort, ofreciéndoles regalos y  promociones, con el fin de convencerlos de que asistan a una presentación de ventas. No se puede invitar a cualquier turista, para determinar si un turista es un posible comprador o no, existen ciertos requisitos o calificaciones que explicaremos más adelante.

2. **La Hostess:** Recibe a los prospectos, se asegura de que estos  clientes comprendan lo que están a punto de hacer y los regalos que van a recibir en agradecimiento por su tiempo. Así mismo, verifica si reúnen los requisitos para participar en la promoción, los califica y los presenta con el liner.
3. **El Liner:**.Una vez que la hostes recibe a los prospectos, la labor del liner consiste en desayunar o comer con los clientes, mostrarles la propiedad y enamorarlos de la membresía, explicándoles todos los beneficios.
4. **El cerrador:** Al terminar el liner con el recorrido por la propiedad, presentara a los clientes con el cerrador, quien les hablara de los precios de las membresías y los financiamientos, presentando el paquete perfecto para finalmente ayudarlos a convertirse en nuevos socios.



> Una pareja de turistas que estaban paseando por las calles de Cancún, sin intenciones de comprar nada, de pronto se encuentran con un muchacho muy amable, que los convence de que participen en una promoción de un resort. Después un representante les habla de las maravillas del lugar y les muestra la mejor manera de vacacionar, Otra persona les ofrece un excelente paquete, imposible de decir que no y de pronto, tenemos que esa pareja de turistas acaban de hacer una inversión de $20,000 dólares, en escasas tres horas...



Sin intenciones de comprar, sin planes de hacerlo, sin haber destinado una cantidad de dinero para hacerlo y mucho menos sin haber ahorrado para eso, se consiguió una venta.

**¡Bienvenido al fascinante mundo de trabajar bajo comisión!** El pago de tus comisiones está basado en la cantidad de posibles compradores (los llamaremos clientes de aquí en adelante), que invites y que atiendan la presentación. ¡Mientras más gente logres que atienda, más dinero en tus bolsillos!

El departamento de promotores juega un papel único en este proceso de ventas, **ya que sin promotores no habría clientes a quienes vender.** Esto nos convierte en un publi-relacionista mas bien que en vendedor. Un publi-rélacionista es alguien que le gusta conocer gente y hacer amigos con el propósito de hacer negocios. Los primeros días tendrás que vencer el miedo y la timidez para poder abordar a los clientes, y cuanto antes lo hagas, cuanto antes comenzarás a ganar **DINERO**.

Tendrás que desarrollar la habilidad de relatar anécdotas, historias, experiencias, etc. (Labia) Deberás desenvolverte con la gente en tu propio estilo. Su reacción hacia ti dependerá de tu empeño. Piensa en cual seria tu reacción si estuvieras en otro país y fueras abordado por alguien desconocido.

Estos son algunos ingredientes básicos, que necesitarás siempre:

**Actitud Mental Positiva:** Si piensas positivamente obtendrás resultados positivos, si piensas negativamente, ¡adivina! no es simplemente una manera de pensar sino una manera de vivir. ¡Sé positivo pase lo que pase! La negatividad lo destruye todo, es tu peor enemigo. La negatividad es la madre de los fracasados.

**Entusiasmo:** La gente con entusiasmo se divierte más. Tu entusiasmo, logrará que la gente se emocione con la promoción que estás realizando. El entusiasmo te obliga a ponerte en acción, aparte de ser contagioso. Esta es la diferencia entre tener un magnifico día o un día aburrido y sin frutos. Sin entusiasmo vas a sonar como una grabadora, aburrirás a tus clientes y nadie te hará caso.

**Dedicación:** Está comprobado que alguien que hace lo que le gusta, lo hace bien. Si eres dedicado, tu trabajo te gustará cada día más. Nadie te va a decir lo que tienes que hacer, siempre tendrás que buscar formas de superarte. Lee buenos libros de motivación y ventas. Dedícate al 100% a tu trabajo. Usa los lapsos de tiempo entre un cliente y otro para mejorar tus argumentos de ventas. Aprende perfectamente las actividades que ofreces (tours, cruceros, etc.) y las calificaciones. Cada vez que no obtengas un cliente pregúntate el por qué y aprende del error para que no te suceda otra vez.

**Responsabilidad:** Si llegas tarde a trabajar, olvidas tu material de trabajo o simplemente no te presentas, los resultados se verán justo en tus bolsillos y obtendrás una muy mala reputación. A la gente de éxito no le gusta relacionarse con gente irresponsable. También deberás de ser responsable con tus clientes, si les prometiste algo deberás cumplirlo.

**Persistencia:** Esta es la habilidad de intentar una y otra vez hasta tener éxito. No cedas. No aceptes un NO por respuesta. Mucha gente se rinde cuando está a punto de alcanzar su meta. Sigue adelante pase lo que pase, usa todos tus recursos hasta que obtengas lo que quieres. Piensa: “Si no obtengo yo a estos clientes, nadie más lo hará por que estoy dando mi 100%” ¡Cada vez que escuchas un NO, te acercas mas a un SI!

**Paciencia:** No esperes aprender todo el trabajo en un día, jamás debes de desesperarte. Las cosas que valen la pena en la vida toman tiempo y esfuerzo, esto significa PACIENCIA. Piensa: "Despacio que voy deprisa:” Cuando estás trabajando, sé paciente, ya vendrán “los buenos”. Aprende a tener paciencia, para que cuando el éxito toque tu puerta, tú estés ahí para recibirlo.

**Se profesional:** Esta es la diferencia entre ser uno de tantos o ser el mejor. Mantén tu locación absolutamente limpia, sin basura. Tu locación es el ejemplo del estado en que se encuentra el resort que estas promoviendo. Cuida tu material de trabajo, locación, mesas, fotos, etc: Si se encuentran en mal estado y sucias, el próximo promotor que trabaje ahí se molestara, (quizá seas tu mismo). Mantén limpieza y pulcritud en tu apariencia. ¿Quién confiaría en ti si te ves sucio, sin rasurar, grasoso, etc.? Usa tu loción favorita, plancha tu uniforme, fájate la camisa, rasúrate, mantén un aliento fresco, etc. ¡Vete bien, sé simpático! ¡Se carismático! ¡Brilla! ¡Adquiere una PERSONALIDAD sobresaliente! |

**Se amable y amigable:** Busca algo en tu cliente que te agrade. Esto hará que sonrías sinceramente. El turismo que nos visita no viene a ver caras aburridas. Una sonrisa siempre gana confianza. Di: *"¡Disculpen!"*, en lugar de *"¡Oigan!"* O *"¡Que tal!"* en lugar de *"¡Hey, ustedes!"*.

Gestos obscenos, palabras impropias, peleas, gritos, etcétera, son características de una persona sin educación. 

El hacer comentarios negativos al público y compañeros únicamente sirve para borrar tu propia buena actitud. Si estás escuchando muchos **NO**, no es el problema de los turistas, *¡el problema es tuyo!* Necesitas cambiar la manera en la que estas promoviendo.

El atender a las juntas de entrenamiento y el hablar con tu gerente o supervisor te ayudará a darte cuenta de lo que estás haciendo mal. Si algún turista expresa algo malo en tu contra, simplemente sonríe y deséale un buen día. Te sentirás bien de que tú no eres el culpable de su frustración y por ningún motivo lo debes de tomar personal. ¡Quizá ni siquiera lo vuelvas a ver en tu vida!

Hay mucha gente que ha perdido su oportunidad por que no entendieron lo que es este negocio. Espera lo mejor del trabajo y actúa inteligentemente, aprende todo en este manual y ponlo en práctica. ¡Ganarás mucho más dinero! 

Después de obtener los conocimientos de este manual y APLICARLOS, se espera que obtengas un MINIMO de 10 a 15 prospectos por semana.

<a name="tecnicismos"></a>
## Tecnicismos del Promotor

**OPC:** Outside Public Contact *(Contacto externo con el público)*. Es un modismo americano para nombrar a los promotores. En este negocio se usa la palabra OPC como el verbo convencer o persuadir. Ejemplo: *"Ayer opicié a esos clientes"*

**Liner:** *(Alineador)* Esta es la persona que da los clientes el recorrido por las instalaciones del resort, explica las ventajas de la membresía, envuelve a los clientes con ella y *los alinea hacia la venta*. Los liners no muestran precios de las membresías.

**Closer:** *(Cerrador)* Esta persona negocia con los clientes justo después de que el liner hace su trabajo. Él es  quien habla al cliente acerca de precios, enganches, financiamientos, etc., hasta que cierra la venta. Algunos desarrollos prefieren nombrar a sus cerradores como *"Finance Manager"* o *"Manager"*

**Front to Back:** Esta persona realiza el trabajo del liner y del closer con un mismo cliente. 

**Front to middle:** Esta persona realiza el trabajo del liner, además de que muestra los precios más altos de las membresías.

**Self-gen:** Esta es la persona que realiza el trabajo del promotor y del liner con un mismo cliente.

**VLO:** *(Verification Legal Officer)* Se encarga de *"limpiar"* posibles malentendidos con los nuevos socios, verificando el contrato de membresías con ellos, y que estos comprendan lo que compraron.

**T/O:** *(Take over)* Acción mediante la cual un liner introduce a un closer con un cliente.

**Double T/O o Tripple:** Acción mediante la cual un closer introduce a otro closer con un cliente.

**Hostess:** Recibe a los prospectos, se asegura de que estos  clientes comprendan lo que están a punto de hacer y los regalos que van a recibir en agradecimiento por su tiempo. Así mismo, verifica si reúnen los requisitos para participar en la promocion, los califica y los presenta con el liner.

**P.R.:** *(Publi-relacionista)* Algunos desarrollos optan por llamar a sus OPC's o promotores o P.R's

**OPC Manager:** Esta es la persona a cargo de la operación del departamento de mercadotecnia.

**PD:** *(Project Director)* Director del Proyecto.

**Timeshare:** *(Tiempo Compartido)* Es la mejor manera de vacacionar en los lugares más hermosos y exclusivos del mundo. Son membresías de vacaciones pre pagadas. Otros nombres son: *CLUB VACACIONAL, VACATION OWNERSHIP, ADVANCE RESERVATION SYSTEM.*

**Resort:** Complejo vacacional que cuenta con todos los servicios que un turista pudiese necesitar durante su estancia.

**Desarrollador:** Propietario del resort.

**ACLUVAQ:** Asociación de Clubes Vacacionales de Quintana Roo.

**AMDETUR:** Asociación Mexicana de Desarrolladores Turísticos.

**ARDA:** American Resort Development Association.

**Out-house:** Programas o locaciones del departamento de mercadotecnia, ubicadas afuera del resort. *(Locación de calle.)*

**In-house:** Programas o locaciones ubicadas dentro del resort o dentro de un centro comercial o restaurante.

**Fly'n Buy / Fly In:** Programas de mercadotecnia enfocados a generar prospectos directamente desde su lugar de origen, ofreciendo una promoción que incluye el vuelo y noches de hospedaje (entre otros regalos).

**Drive In:** Similar al anterior pero por su cercana situación geográfica, los prospectos manejan su propio automóvil para llegar al resort.

**Telemarketing / Box Program / Mailing:** Programas de mercadotecnia enfocados a generar prospectos vía telefónica (previa autorización de los prospectos)

**Off-Site:** Es un programa que establece una sala de ventas y un equipo de mercadotecnia en una ciudad distinta al destino turístico donde se ubica el resort. Los programas de generación de prospectos se basan en telemarketing.

**Pitch:** *(discurso)* Argumento de ventas preestablecido, utilizado para convencer.

**Meter parejas:** Prospectar.

**Opicear:** Acción de meter parejas. También se utiliza como verbo: _"Ayer opicié a esa pareja..."_

**Güasar:** Cuando existe más de un promotor en una locación, toman turnos para abordar a las parejas. Güasar es apartar a una pareja que viene caminando, para abordarla por el promotor que la _"güasó"_. Otros promotores simplemente avisan: _"Voy con el señor de la cámara."_ Lo cual significa que se le  respetará su turno de abordarlos.

**Voy soltando:** Cuando un promotor siente que otro está perdiendo el control sobre una pareja, este le indica que al terminar, sino logra obtenerlos, él los abordara. 

**Q's / Qualified Units:** Son los turistas que cumplieron con todos los requisitos para participar en la promoción. Tus comisiones se basan en la cantidad de Q's que obtengas.

**N.Q's / Not Qualified:** Son los turistas que no reúnen los requisitos o calificaciones para participar en la promoción. Tú no cobras comisiones por este concepto.

**Up's:** Turistas que fueron invitados y se presentaron al resort (Qs y N.Q 's).

**Eficiencia:** Volumen de ventas dividido entre la cantidad de Q's. Esto es el termómetro del desempeño de un promotor, una locación, un programa, un liner, un cerrador, etc. _Baja eficiencia = fuera del negocio_

**Show rate:** Porcentaje de parejas invitadas a la presentación, que se presentaron a la misma. Un porcentaje; bajo, significa que el promotor tiene que mejorar su cierre.

**Requal's:** Prospectos que por alguna omision o error resultaron N.Q.'s y son cambiadas a Q's

**Strokers:** Aprovechados o _"gorrones"_, Clientes que quieren todo gratis o precios aún más bajos a los que ya se ofrecen, en el medio, también se utiliza como verbo.

**Lay Downs:** Estos son los clientes más fáciles, que dicen si todo y se venden a sí mismos. Clientes a quienes resulta fácil obtener, _¡Los más apreciados por todos!_

**Survey Sheet:** _Hoja de Encuesta._ Es un pequeño cuestionario que  los liners tienen que llenar al comienzo de la presentación. Esto determina si los clientes son Q o no. Ademas, es un arma que tiene el cerrador para determinar el que ofrecerles, cuanto ofrecerles, etc.

**Welcome Sheet:** Hoja de bienvenida que llena la hostess al recibir a los clientes y que sirve de comprobante para los promotores.

**Converter:** _(Convertidor, gancho)_ Es el regalo o descuento que das a tus clientes a cambio de asistir a la presentación.

**AMP:** Actitud Mental Positiva.

**Booking:** Turistas invitados a la presentación para el día de mañana u otro día.

**Depósito:** Cantidad de dinero que los turistas invitados a la presentación para una fecha/hora posterior, pagaron para reservaciones de tours o servicios. El obtener depósitos compromete más a los clientes. Estos son obtenidos por promotores que trabajan en el turno de la tarde, e invitan a sus clientes para el día siguiente.

**Abierta (Invitación):** Turistas invitados a la presentación para una fecha/hora posterior, quienes no dejaron depósito o no están comprometidos a cierta hora o fecha. Estos turistas casi nunca asisten, así es que siempre obtén el depósito.

**Directa:** Turistas invitados a presentarse al momento. Estos clientes son llevados en taxi personalmente por el promotor que trabaja en el turno de la mañana.

**Pick Up:** Para asegurarte de que tus clientes asistan a la presentación y lleven sus identificaciones y las tarjetas de crédito con las que califican como pareja Q,  ofrecerás el servicio de **"Pick up"** _(pasar por ellos)_. Irás a recogerlos personalmente a su hotel y los llevarás a la presentación. Durante el transcurso al hotel puedes ir relajandolos para que lleguen al hotel con mente abierta.

**Walked outs / Bolted:** Prospectos que no finalizaron la presentación y se fueron del resort antes de tiempo. Todos estos serán N.Q.'s

**In and Out:** Prospectos que por no estar bien informados por el promotor, deciden no entrar a la presentación al momento de estar siendo registrados por la hostess.

**Invitación quemada:** Existen dos clases de invitaciones quemadas, las que tu quemas y las que te queman  a ti. Una invitación quemada es cuando un promotor invita a un cliente que ya tenía alguna otra invitación de algún otro promotor, _"quemando"_ al cliente para que no asista con su invitación original y asista a cambio con su nueva invitación. Por lo general este es el resultado de un trabajo mal hecho por el primer promotor.

**Parejas quemadas:** Estos son los clientes que ya han asistido a una o varias presentaciones y han ocupado mucho tiempo de sus vacaciones en las mismas. Por lo tanto, ya no desean asistir a más. También son aquellos clientes que han escuchado a alguien hablar mal del tiempo compartido, como por ejemplo algunos representantes de agencias de tours.


**Briefing:** Es una presentacion que dan los Reps de agencias de viajes a los grupos de turistas que ellos captan para la plaza. Esta presentación es para informarles acerca del lugar y sus alrededores, para vender boletos para las actividades (tours, cruceros, shows, etc) y para asistirlos con cualquier problema que puedan tener durante su estancia. Normalmente, los Reps aprovechan el briefing para _"quemar"_ al turismo,  advirtiéndoles de no asistir a una presentación de tiempo compartido.

**Wake Up Call:** _Llamada despertador_ Muchos clientes aprecian que los despierten, ofrece esta llamada. Te ayudará a confirmar la asistencia de tus clientes y podrás asegurarte de que lleguen a tiempo. También podrás enterarte si tus clientes enfermaron o tuvieron algún problema, en estos casos, la llamada de confirmación te ayudará a hacer un seguimiento de tus clientes.

**Línea (Horario):** Periodo de tiempo en el que se recibe a los clientes en el resort para la presentación.

**Línea (Liner):** Argumento de ventas del liner.

**Romper el pacto:** Acción mediante la cual el liner solicita a los prospectos un compromiso de honestidad, al principio de la presentación. Dicha acción abre la mente de los clientes al hecho de que se les va a solicitar que compren la membresía,

**Top Booker:** Es un promotor sobresaliente, que al finalizar una semana, un mes o un año obtuvo mas Q 's que el resto del equipo.

**Top Seller:** Es un promotor sobresaliente, que al finalizar una semana, un mes o un año obtuvo el mayor volumen de ventas.

**Blanquear:** _Irse en blanco._ Sin producción por un día o semana. Es el reflejo de la negatividad, la falta de ganas, el conformismo y la mediocridad.

**Venta o Deal:** Clientes que compraron la membresía de tiempo compartido.

**Venta Procesable:** Cuando las personas pagan la cantidad total del enganche necesario para comprar la membresía, Cobrarás tus comisiones al tener una venta procesable.

**Venta Pending o Pender:** Cuando el enganche no esta pagado en su totalidad o el pago esta postfechado. No cobrarás comisiones en tanto no se procese la venta.

**Venta Mickey Mouse:** Venta de fantasía, Cuando el cliente compra la membresía pero paga una cantidad muy pequeña o nada de dinero, aunque firme contrato.

**Exit / Tri:** Venta de una membresía muy pequeña cómo "paquete de prueba".

**Cancelación:** Ventas canceladas.

**Maniflesto:** Es la hoja de registro de los resultados del día de la sala de ventas; Nombre de los clientes, hora de llegada, nombre del promotor que los invitó, nombre del liner, nombre del closer, cantidad de la venta, etc. Es la hoja de control donde podrás verificar cuantos de tus clientes atendieron a la presentación, si fueron Q's o no y si compraron o no.

**Pre-Manifiesto:** Es la hoja de reservaciones en donde se reporta a todos los clientes que asistirán a la presentación al día siguiente.

**Spiffs:** Bonos o incentivos adicionales a tus comisiones, que han sido ganados por un desempeño especial al alcanzar determinadas metas. Por ejemplo, tu gerente podria decir: _¡Quien sea que ontenga tres Q hoy, ganara un spiff de $50 USD"

**Pitch Book:** Es un álbum de fotos diseñado especialmente para dar un soporte visual a tu pitch.

**Charge Back:** Cargos. Cuando ofreces a tu cliente regalos o descuentos adicionales a lo permitido, se te descontará de tus comisiones la cantidad excedida.

**Quinelas o Quanelas:** Dos parejas viajando juntas.

**Trinellas:** Tres parejas viajando juntas.

**Split Quinela / Trinella:** Cuando un promotor indebidamente separa a un grupo de turistas que están viajando juntos, para obtener doble comisión, es muy fácil de detectar.



```bash
$ tesseract X.jpg OPC_1 -l spa
$ pandoc book.markdown -o book.epub
```